User.create!(name:  "SeiraOkumura",
             email: "seira@insta.org",
             password:              "password2",
             password_confirmation: "password2",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@insta.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

users = User.order(:created_at)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.instaposts.create!(content: content) }
end