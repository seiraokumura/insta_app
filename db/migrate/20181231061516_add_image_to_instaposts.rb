class AddImageToInstaposts < ActiveRecord::Migration[5.1]
  def change
    add_column :instaposts, :image, :string
  end
end
