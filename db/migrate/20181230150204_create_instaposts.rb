class CreateInstaposts < ActiveRecord::Migration[5.0]
  def change
    create_table :instaposts do |t|
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :instaposts, [:user_id, :created_at]
  end
end