require 'test_helper'

class InstapostTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @instapost = @user.instaposts.build(content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @instapost.valid?
  end

  test "user id should be present" do
    @instapost.user_id = nil
    assert_not @instapost.valid?
  end
  
  test "content should be at most 140 characters" do
    @instapost.content = "a" * 141
    assert_not @instapost.valid?
  end
  
  test "order should be most recent first" do
    assert_equal instaposts(:most_recent), Instapost.first
  end
end
